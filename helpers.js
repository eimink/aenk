const videoFilter = function(req, file, cb) {
	if (!file.originalname.match(/\.(mp4|MP4|mov|MOV|avi|AVI|mkv|MKV)$/)) {
		req.fileValidationerror = 'Only video files are allowed!';
		return cb(new Error(req.fileValidationerror), false);
	}
	cb(null, true);
};
exports.videoFilter = videoFilter;
