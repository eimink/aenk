const express = require('express');
const multer = require('multer');
const path = require('path');
const helpers = require('./helpers');
const normalize = require('ffmpeg-normalize');

const app = express();

const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/processed'));

const storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, 'uploads/');
	},
	filename: function(req, file, cb) {
		console.log(file.mimetype);
		cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
	}
});

app.listen(port, () => console.log(`AENK listening on port ${port}`));

app.post('/normalize', (req, res) => {
	let upload = multer({ storage: storage, fileFilter: helpers.videoFilter }).single('video_file');
	upload (req, res, function(err) {
		if (req.fileValidationError) {
			return res.send(req.fileValidationError);
		}
		else if (!req.file) {
			return res.send('Please select a video to upload');
		}
		else if (err instanceof multer.MulterError) {
			return res.send(JSON.stringify(err));
		}
		else if (err) {
			return res.send(JSON.stringify(err));
		}
		else
		{	
			var filename = "normalized_"+req.file.originalname;
			normalize({
				input:'uploads/'+req.file.filename,
				output:'processed/normalized_'+req.file.originalname,
				loudness: {
					normalization: 'ebuR128',
					target:
					{
						input_i: -23,
						input_lra: 7.0,
						input_tp: -2.0
					}
				}
			})
			.then(normalized => {
				res.send("Normalization succesful! <a href='processed/"+filename+"'>download</a>");
			})
			.catch(error => {
				res.send(`Error:` + JSON.stringify(error));
			});
		}
	});
});
